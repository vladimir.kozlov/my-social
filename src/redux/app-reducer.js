import {authAPI} from '../api/api'
import {stopSubmit} from 'redux-form'
import{getAuthUserData} from './auth-reducer'

const INITIALIZED_SUCCESS = 'my-social/app/INITIALIZED-SUCCESS'

let initialState = {
  initialized: false
}

const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case INITIALIZED_SUCCESS: {
      return {
        ...state,
        initialized: true
        }
      }

    default:
      return state
  }
}

export const initializedSuccess = () => ({type: INITIALIZED_SUCCESS})

export const initializeApp = () => (dispatch) => {
  const promise = dispatch(getAuthUserData())
  Promise.all([promise])
    .then(() => {
      dispatch(initializedSuccess())
  })
}

export default AppReducer
