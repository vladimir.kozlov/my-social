import {userAPI, profileAPI} from '../api/api'
import {stopSubmit} from 'redux-form'

const ADD_POST = 'my-social/profile/ADD-POST'
const UPDATE_NEW_POST_TEXT = 'my-social/profile/UPDATE-NEW-POST-TEXT'
const SET_USER_PROFILE = 'my-social/profile/SET-USER-PROFILE'
const SET_STATUS = 'my-social/profile/SET-STATUS'
const DELETE_POST = 'my-social/profile/DELETE-POST'
const SAVE_PHOTO_SUCCESS = 'my-social/profile/SAVE-PHOTO-SUCCESS'

let initialState = {
  posts: [
    {id: 1, message: 'Hi, how are you', likeCount: 12},
    {id: 2, message: 'It is my post 1!', likeCount: 33}
  ],
  newPostText: 'new post text',
  profile: null,
  status: ''
}

const ProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_POST: {
      let newPost = {
        id: 5,
        message: action.newPostText,
        likeCount: 0
      }
      return {
        ...state,
        posts: [...state.posts, newPost],
        newPostText: ''
      }
    }

    case SET_USER_PROFILE: {
      return {
        ...state,
        profile : action.profile
      }
    }

    case SET_STATUS: {
      return {
        ...state,
        status : action.status
      }
    }

    case DELETE_POST: {
      return {
        ...state,
        posts: state.posts.filter(p => p.id != action.postId)
      }
    }

    case SAVE_PHOTO_SUCCESS: {
      return {
        ...state,
        profile: {...state.profile, photos: action.photos}
      }
    }

    default:
      return state
  }
}

export const addPost = (newPostText) => ({type: ADD_POST, newPostText})
export const setUserProfile = (profile) => ({type: SET_USER_PROFILE, profile})
export const setStatus = (status) => ({type: SET_STATUS, status})
export const deletePost = (postId) => ({type: DELETE_POST, postId})
export const savePhotoSuccess = (photos) => ({type: SAVE_PHOTO_SUCCESS, photos})

export const getUserProfile = (userId) => async (dispatch) => {
  const response = await userAPI.getProfile(userId)

  dispatch(setUserProfile(response.data))
}

export const getStatus = (userId) => async (dispatch) => {
  const response = await profileAPI.getStatus(userId)

  dispatch(setStatus(response.data))
}

export const updateStatus = (status) => async (dispatch) => {
  const response = await profileAPI.updateStatus(status)

  if (response.data.resultCode === 0) {
    dispatch(setStatus(status))
  }
}

export const savePhoto = (file) => async (dispatch) => {
  const response = await profileAPI.savePhoto(file)

  if (response.data.resultCode === 0) {
    dispatch(savePhotoSuccess(response.data.data.photos))
  }
}

export const saveProfile = (profile) => async (dispatch, getState) => {
  const userId = getState().auth.userId
  const response = await profileAPI.saveProfile(profile)

  if (response.data.resultCode === 0) {
    dispatch(getUserProfile(userId))
  } else {
    const message = response.data.messages.length > 0 ? response.data.messages[0] : 'Something was wrong'
    dispatch(stopSubmit('edit-profile', {_error: message}))
    return Promise.reject(message)
  }
}

export default ProfileReducer
