import {applyMiddleware, compose, combineReducers, createStore} from 'redux'
import ProfileReducer from './profile-reducer'
import DialogsReducer from './dialogs-reducer'
import SidebarReducer from './sidebar-reducer'
import UsersReducer from './users-reducer'
import AuthReducer from './auth-reducer'
import AppReducer from './app-reducer'
import thunkMiddleware from 'redux-thunk'
import { reducer as FormReducer } from 'redux-form'

let reducers = combineReducers({
  profilePage: ProfileReducer,
  dialogsPage: DialogsReducer,
  sidebar: SidebarReducer,
  usersPage: UsersReducer,
  auth: AuthReducer,
  form: FormReducer,
  app: AppReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(reducers, composeEnhancers(
    applyMiddleware(thunkMiddleware)
  ))

export default store
