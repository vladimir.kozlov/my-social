import ProfileReducer, {addPost, deletePost} from './profile-reducer'
import React from 'react'

let state = {
  posts: [
    {id: 1, message: 'Hi, how are you', likeCount: 12},
    {id: 2, message: 'It is my post 1!', likeCount: 33}
  ]
}

it('checking length of messages after adding post', () => {
  let action = addPost('add new post')

  let newState = ProfileReducer(state, action)

  expect(newState.posts.length).toBe(3)
})

it('checking new post message value', () => {
  let action = addPost('add new post')

  let newState = ProfileReducer(state, action)

  expect(newState.posts[2].message).toBe('add new post')
})

it('checking length of messages after deleting post', () => {
  let action = deletePost(1)

  let newState = ProfileReducer(state, action)

  expect(newState.posts.length).toBe(1)
})
