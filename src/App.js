import React from 'react'
import './App.css'
import HeaderContainer from './components/Header/HeaderContainer'
import Navbar from './components/Navbar/Navbar'
import Login from './components/Login/Login'
import {Route, withRouter, BrowserRouter, Switch, Redirect} from 'react-router-dom'
import store from './redux/redux-store'
import {Provider} from 'react-redux'
import {connect} from 'react-redux'
import {initializeApp} from './redux/app-reducer'
import {compose} from 'redux'
import Preloader from './components/common/Preloader/Preloader'
import {WithSuspense} from './hoc/WithSuspense'

const DialogsContainer = React.lazy(() => import('./components/Dialogs/DialogsContainer'))
const ProfileContainer = React.lazy(() => import('./components/Profile/ProfileContainer'))
const UsersContainer = React.lazy(() => import('./components/Users/UsersContainer'))

class App extends React.Component {
  componentDidMount() {
    this.props.initializeApp()
  }

  render() {
    if (!this.props.initialized) {
      return <Preloader />
    }

    return (
        <div className='app-wrapper'>
          <HeaderContainer />
          <Navbar />
          <div className='app-wrapper-content'>

          <Switch>
            <Route exact path='/'
              render={() => <Redirect to='/profile' />} />

              <Route path='/dialogs'
                render={WithSuspense(DialogsContainer)} />

              <Route path='/profile/:userId?'
                render={WithSuspense(ProfileContainer)} />

              <Route path='/users'
                render={WithSuspense(UsersContainer)} />

              <Route path='/login'
                render={ () => <Login /> } />

              <Route path='*'
                render={ () => <div>404 NOT FOUND</div> } />
            </Switch>
          </div>
        </div>
    )
  }
}

const mapStateToProps = (state) => ({
  initialized: state.app.initialized
})

let AppContainer = compose(
  withRouter,
  connect(mapStateToProps, {initializeApp}))
  (App)

const MySocialApp = (props) => {
  return <BrowserRouter>
    <Provider store={store}>
      <AppContainer />
    </Provider>
  </BrowserRouter>
}

export default MySocialApp
