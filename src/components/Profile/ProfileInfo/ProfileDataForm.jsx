import React from 'react'
import {reduxForm} from 'redux-form'
import {createField, Input, TextArea} from '../../common/FormsControls/FormsControls'
import formStyle from '../../common/FormsControls/FormsControls.module.css'
import s from './ProfileInfo.module.css'

const ProfileDataForm = ({handleSubmit, profile, error}) => {
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <button>save</button>
      </div>
      { error && <div className={formStyle.formSummaryError}>
        {error}
      </div> }
      <div>
        <b>Full name</b>: {createField('Full name', 'fullName', [], Input)}
      </div>
      <div>
        <b>Looking for a job</b>: {createField('', 'lookingForAJob', [], Input, {type: 'checkbox'})}
      </div>
        <div>
          <b>My skills</b>: {createField('My skills', 'lookingForAJobDescription', [], TextArea)}
        </div>
      <div>
        <b>About me</b>: {createField('About me', 'aboutMe', [], TextArea)}
      </div>
      <div>
        <b>Contacts</b>: {Object.keys(profile.contacts).map(key => {
          return <div className={s.contact} key={key}>
            <b>{key}</b>: {createField(key, 'contacts.' + key, [], Input)}
          </div>
        })}
      </div>
    </form>
  )
}

const Contact = ({contactTitle, contactValue}) => {
  return <div className={s.contact}>
    <b>{contactTitle}</b>: {contactValue}
  </div>

}

const ProfileDataReduxForm = reduxForm({
  form: 'edit-profile'
})(ProfileDataForm)

export default ProfileDataReduxForm
