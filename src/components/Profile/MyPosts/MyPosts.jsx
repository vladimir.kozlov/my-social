import React from 'react'
import s from './MyPosts.module.css'
import Post from './Post/Post'
import {addPost} from '../../../redux/profile-reducer'
import {Field, reduxForm} from 'redux-form'
import {maxLengthCreator, required} from '../../../utils/validators/validators'
import {TextArea} from '../../common/FormsControls/FormsControls'

const MyPosts = (props) => {
  let postElements = props.posts.map(post => <Post key={post.id} profile={props.profile} message={post.message} likeCount={post.likeCount} />)

  let newPostElement = React.createRef()

  let addPost = (values) => {
    props.addPost(values.newPostText)
  }

  return (
    <div className={s.postsBlock}>
      <h3>My Posts</h3>
      <AddPostFormRedux onSubmit={addPost} />
      <div className={s.posts}>
        {postElements}
      </div>
    </div>
  )
}

const maxLength10 = maxLengthCreator(10)

let AddPostForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit} >
      <div>
        <Field name='newPostText' component={TextArea} placeholder='PostMessage'
          validate={[required, maxLength10]} />
      </div>
      <div>
        <button>Add post</button>
      </div>
    </form>
  )
}

const AddPostFormRedux = reduxForm({
  form: 'profileAddPostForm'
})(AddPostForm)

export default MyPosts
