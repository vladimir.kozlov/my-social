import React from 'react'
import s from './MyPosts.module.css'
import MyPosts from './MyPosts'
import {addPost} from '../../../redux/profile-reducer'
import {connect} from 'react-redux'

let mapStateToProps = (state) => {
  return {
    posts: state.profilePage.posts,
    newPostText: state.profilePage.newPostText,
    profile: state.profilePage.profile
  }
}

const MyPostsContainer = connect(mapStateToProps, {addPost})(MyPosts)

export default MyPostsContainer
