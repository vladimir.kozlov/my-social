import React from 'react'
import s from './Header.module.css'
import {NavLink} from 'react-router-dom'
import headerLogo from '../../assets/images/headerLogo.png'

const Header = ({isAuth, login, logout}) => {
  return (
    <header className={s.header}>
      <img src={headerLogo} />
      <div className={s.loginBlock}>
        { isAuth
          ? <div>{login} - <button onClick={logout}>Logout</button></div>
          : <NavLink to='/login'>Login</NavLink> }
      </div>
    </header>
  )
}

export default Header
