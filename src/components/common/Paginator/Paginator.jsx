import React, {useState} from 'react'
import s from './Paginator.module.css'
import cn from 'classnames'

let Paginator = ({totalItemsCount, pageSize, currentPage, onPageChanged, blockSize = 10}) => {
  let pagesCount = Math.ceil(totalItemsCount / pageSize)
  let pages = []
  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i)
  }

  let blockCount = Math.ceil(pagesCount / blockSize)
  let [blockNumber, setBlockNumber] = useState(1)
  let leftBlockPageNumber = (blockNumber - 1) * blockSize + 1
  let rightBlockPageNumber = blockNumber * blockSize + 1

  return (
    <div className={s.paginator}>
      { blockNumber > 1 &&
      <button onClick={ () => {setBlockNumber(blockNumber - 1)} }>PREV</button>}

      {pages
        .filter(p => p >= leftBlockPageNumber && p <= rightBlockPageNumber)
        .map(p => {
          return <span className={cn({
            [s.selectedPage]: currentPage === p && s.selectedPage
          }, s.pageNumber) }
          key={p}
          onClick={(e) => { onPageChanged(p) }}>{p}</span>
        })
      }

      { blockCount > blockNumber &&
      <button onClick={ () => {setBlockNumber(blockNumber + 1)} }>NEXT</button>}
    </div>
  )
}

export default Paginator
