import React from 'react'
import s from './Dialogs.module.css'
import DialogItem from './DialogItem/DialogItem'
import Message from './Message/Message'
import {Field, reduxForm} from 'redux-form'
import {maxLengthCreator, required} from '../../utils/validators/validators'
import {TextArea} from '../common/FormsControls/FormsControls'

const Dialogs = ({dialogsPage, sendMessage}) => {
  let state = dialogsPage

  let dialogElements = state.dialogs.map(dialog => <DialogItem name={dialog.name} key={dialog.id} id={dialog.id} />)
  let messageElements = state.messages.map(message => <Message message={message.message} key={message.id}/>)
  let newMessageBody = state.newMessageBody

  let addNewMessage = (values) => {
    sendMessage(values.newMessageBody)
  }

  return (
    <div className={s.dialogs}>
      <div className={s.dialogItems}>
        {dialogElements}
      </div>
      <div className={s.messages}>
        <div>
          {messageElements}
        </div>
        <AddMessageFormRedux onSubmit={addNewMessage} />
      </div>
    </div>
  )
}

const maxLength50 = maxLengthCreator(50)

const AddMessageForm = ({handleSubmit}) => {
  return (
    <form onSubmit={handleSubmit} >
      <div>
        <Field name='newMessageBody' component={TextArea} placeholder='Enter message'
          validate={[required, maxLength50]} />
      </div>
        <button>Send</button>
      <div>
      </div>
    </form>
  )
}

const AddMessageFormRedux = reduxForm({
  form: 'dialogAddMessageForm'
})(AddMessageForm)

export default Dialogs
