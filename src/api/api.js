import * as axios from 'axios'

const instanse = axios.create({
  baseURL: 'https://social-network.samuraijs.com/api/1.0/',
  withCredentials: true,
  headers: {
    'API-KEY': '22ac2370-415b-49bf-a9b5-85dddc1ffc79'
  }
})

export const userAPI = {
  getUsers(currentPage, pageSize) {
    return instanse.get(`users?page=${currentPage}&count=${pageSize}`).then(response => {
        return response.data
    })
  },
  follow(userId) {
    return instanse.post(`follow/${userId}`)
  },
  unfollow(userId) {
    return instanse.delete(`follow/${userId}`)
  },
  getProfile(userId) {
    console.warn('Obsolete method. Please use profileAPI')
    return profileAPI.getProfile(userId)
  },
  authMe() {
    return instanse.get(`auth/me`)
  }
}

export const profileAPI = {
  getProfile(userId) {
    return instanse.get(`profile/${userId}`)
  },
  getStatus(userId) {
    return instanse.get(`profile/status/${userId}`)
  },
  updateStatus(status) {
    return instanse.put(`profile/status`, {
      status: status
    })
  },
  savePhoto(photoFile) {
    let formData = new FormData()
    formData.append('image', photoFile)

    return instanse.put(`profile/photo`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },
  saveProfile(profile) {
    return instanse.put(`profile`, profile)
  }
}

export const authAPI = {
  me() {
    return instanse.get(`auth/me`)
  },
  login(email, password, rememberMe = false, captcha = null) {
    return instanse.post(`auth/login`, {email, password, rememberMe, captcha})
  },
  logout() {
    return instanse.delete(`auth/login`)
  }
}

export const securityAPI = {
  getCaptchaUrl() {
    return instanse.get(`security/get-captch-url`)
  }
}
